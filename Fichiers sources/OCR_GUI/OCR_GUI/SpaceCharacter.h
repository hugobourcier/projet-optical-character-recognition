#ifndef SPACECHARACTER_H
#define SPACECHARACTER_H

#include "CharacterObject.h"

//--------------------------//
// Classe SpaceCharacter	//
//							//
// Indique la pr�sence		//
// d'un espace				//
// dans le vecteur de 		//
// de caract�res.			//
//--------------------------//

class SpaceCharacter : public CharacterObject
{
public:
	SpaceCharacter();
	~SpaceCharacter();
};

#endif //SPACECHARACTER_H
